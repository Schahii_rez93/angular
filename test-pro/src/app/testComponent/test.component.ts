
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app_test',
  templateUrl: "./test.component.html"
})

export class TestComponent {
  allowNewS: boolean = false;
  status: string = "turn On";
  text: string = '';

  create: boolean = true;

  constructor() {
    setTimeout(() => {
      this.allowNewS = true;
      }, 2000);

    this.status = Math.random() > 0.5 ? 'UnClicked!' : 'Clicked!';
  }

  OnCreater(){
      this.create = !this.create;
  }

  // updateText(event: Event) {
  //   this.text = (<HTMLInputElement>event.target).value;
  // }

  getStatus(){
    return this.status;
  }

  getColor(){
    return this.status === "UnClicked!" ? "gray" : "green";
  }
}
