import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {FormsModule} from '@angular/forms';

import {headerComponent} from './headerComponent/header.component';
import {MainComponent} from './mainComponent/Main.component';
import {MainTextComponent} from './mainTextComponent/mainText.component';
import {MainImgComponent} from './mainImageComponent/mainImg.component';
import {FooterComponent} from './footerComponent/footer.component';

import {TestComponent} from './testComponent/test.component';

@NgModule({
  declarations: [
    AppComponent,
    headerComponent,
    MainComponent,
    MainTextComponent,
    MainImgComponent,
    FooterComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
