
import {Component} from "@angular/core";

@Component({
  selector: 'app-header',
  templateUrl: "./header.component.html"
})

export class headerComponent {
  serverId: number = 4;
  serverStatus: string = 'online';

  getServerStatus(){
    return this.serverStatus;
  }

  btnEnable: boolean = false;
  btnColor: string = "red";

  constructor() {
    setTimeout(() => {
      this.btnEnable = true;
      this.btnColor = "gray";
    }, 2000);
  }
}
