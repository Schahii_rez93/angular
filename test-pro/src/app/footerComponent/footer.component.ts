
import {Component} from '@angular/core';

@Component({
  selector: 'app_footer',
  templateUrl: "./footer.component.html",
})

export class FooterComponent {
    newCreate: boolean = false;
    createStatus: string = "false";

    constructor() {
      setTimeout(() => {
        this.newCreate = true;
        }, 2000);
    }
}
